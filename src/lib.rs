const N_COLS: usize = 7;
const N_ROWS: usize = 6;
const HEIGHT: usize = N_ROWS + 1;
const COL_MASK: u64 = (1 << HEIGHT) - 1;
const DIRECTIONS: [usize; 4] = [
    1,          // Vertical
    HEIGHT,     // Horizontal
    HEIGHT - 1, // Diagonal /
    HEIGHT + 1, // Diagonal \
];

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Token {
    One,
    Two,
}

#[derive(Debug)]
pub struct Board {
    bitboard1: u64,
    bitboard2: u64,
    current_turn: Token,
}

impl Default for Board {
    fn default() -> Self {
        Self::new()
    }
}

impl Board {
    pub fn new() -> Board {
        Board {
            bitboard1: 0,
            bitboard2: 0,
            current_turn: Token::One,
        }
    }

    fn has_4_connected(tokens: u64) -> bool {
        DIRECTIONS.into_iter().any(|dir| {
            let temp = tokens & tokens << dir;
            temp & (temp << (2 * dir)) != 0
        })
    }

    fn get_token_unchecked(&self, row_0: usize, column_0: usize) -> Option<Token> {
        let mask = 1 << (column_0 * HEIGHT + row_0);
        if self.bitboard1 & mask != 0 {
            Some(Token::One)
        } else if self.bitboard2 & mask != 0 {
            Some(Token::Two)
        } else {
            None
        }
    }

    pub fn get_row(&self, row: usize) -> [Option<Token>; N_COLS] {
        assert!((1..=N_ROWS).contains(&row));
        let row = row - 1;
        let mut res = [None; N_COLS];
        for (column, item) in res.iter_mut().enumerate() {
            *item = self.get_token_unchecked(row, column);
        }
        res
    }

    pub fn get_column(&self, column: usize) -> [Option<Token>; N_ROWS] {
        assert!((1..=N_COLS).contains(&column));
        let column = column - 1;
        let mut res = [None; N_ROWS];
        for (row, item) in res.iter_mut().enumerate() {
            *item = self.get_token_unchecked(row, column);
        }
        res
    }

    pub fn get_valid_moves(&self) -> Vec<usize> {
        let bitboard = self.bitboard1 | self.bitboard2;
        (0..N_COLS)
            .filter(|col| bitboard & (1 << (col * HEIGHT + N_ROWS - 1)) == 0)
            .map(|col| col + 1)
            .collect()
    }
    pub fn current_turn(&self) -> Token {
        self.current_turn
    }

    pub fn play(self, column: usize) -> Board {
        assert!((1..=N_COLS).contains(&column));
        let column = column - 1;
        let column_state = ((self.bitboard1 | self.bitboard2) >> (column * HEIGHT)) & COL_MASK;
        let new_bit = column_state + 1;
        assert!(new_bit < 1 << N_ROWS);
        let new_bit = new_bit << (column * HEIGHT);
        let current_turn = self.current_turn;
        match current_turn {
            Token::One => Board {
                bitboard1: self.bitboard1 | new_bit,
                bitboard2: self.bitboard2,
                current_turn: Token::Two,
            },
            Token::Two => Board {
                bitboard1: self.bitboard1,
                bitboard2: self.bitboard2 | new_bit,
                current_turn: Token::One,
            },
        }
    }

    pub fn winner(&self) -> Option<Token> {
        if Board::has_4_connected(self.bitboard1) {
            Some(Token::One)
        } else if Board::has_4_connected(self.bitboard2) {
            Some(Token::Two)
        } else {
            None
        }
    }

    pub fn draw(&self) {
        for row in (1..self.get_column(1).len() + 1).rev() {
            for token in self.get_row(row) {
                print!(
                    "|{}",
                    match token {
                        Some(Token::One) => "O",
                        Some(Token::Two) => "X",
                        _ => "_",
                    }
                );
            }
            println!("|");
        }
        for col in 1..self.get_row(1).len() + 1 {
            print!(" {}", col);
        }
        println!(" ");
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn determines_winner() {
        let games = vec![
            (vec![1, 2, 1, 2, 1, 2, 1], Some(Token::One)),
            (vec![3, 2, 1, 2, 1, 2, 1, 2], Some(Token::Two)),
            (vec![1, 2, 2, 3, 3, 4, 3, 4, 4, 1, 4], Some(Token::One)),
            (vec![1, 2, 2, 3, 3, 4, 3, 4, 5, 4, 4], Some(Token::One)),
        ];
        for (moves, winner) in games {
            let mut board = Board::new();
            for col in moves {
                assert!(board.winner() == None);
                board = board.play(col);
            }
            assert!(board.winner() == winner);
        }
    }
}
