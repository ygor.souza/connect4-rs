use connect4::Board;
use std::io;

fn main() {
    let mut board = Board::new();
    println!("Playing Connect Four");
    while board.winner().is_none() {
        board.draw();
        let moves = board.get_valid_moves();
        if moves.is_empty() {
            break;
        }
        println!("Player {:?} move ({:?}):", board.current_turn(), moves);
        let mut column = String::new();
        io::stdin()
            .read_line(&mut column)
            .expect("Failed to read line");
        let column = match column.trim().parse::<usize>() {
            Ok(col) if moves.contains(&col) => col,
            _ => {
                println!("Invalid move");
                continue;
            }
        };
        board = board.play(column);
    }
    board.draw();
    println!("Winner: {:?}", board.winner());
}
